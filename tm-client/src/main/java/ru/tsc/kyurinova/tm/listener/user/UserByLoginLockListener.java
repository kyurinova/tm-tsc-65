package ru.tsc.kyurinova.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.event.ConsoleEvent;
import ru.tsc.kyurinova.tm.listener.AbstractListener;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

@Component
public class UserByLoginLockListener extends AbstractListener {

    @NotNull
    @Override
    public String command() {
        return "user-lock-by-login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "lock user by login...";
    }

    @Override
    @EventListener(condition = "@userByLoginLockListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        adminUserEndpoint.lockUserByLogin(sessionService.getSession(), login);
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }
}
