package ru.tsc.kyurinova.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.endpoint.SessionDTO;

public interface ISessionService {

    @Nullable
    SessionDTO getSession();

    void setSession(@Nullable final SessionDTO session);


}
