package ru.tsc.kyurinova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kyurinova.tm.api.service.model.IOwnerService;
import ru.tsc.kyurinova.tm.exception.empty.EmptyIdException;
import ru.tsc.kyurinova.tm.exception.empty.EmptyIndexException;
import ru.tsc.kyurinova.tm.exception.empty.EmptyUserIdException;
import ru.tsc.kyurinova.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kyurinova.tm.model.AbstractOwnerModel;
import ru.tsc.kyurinova.tm.repository.model.AbstractOwnerRepository;

import java.util.List;

public abstract class AbstractOwnerService<M extends AbstractOwnerModel>
        extends AbstractService<M> implements IOwnerService<M> {

    @NotNull
    @Autowired
    private AbstractOwnerRepository<M> repository;

    @Transactional
    public void remove(@Nullable String userId, @Nullable M model) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (model == null) throw new EntityNotFoundException();
        if (model.getId() == null || model.getUser().getId() == null) return;
        repository.delete(model);
    }

    public @NotNull List<M> findAll(@Nullable String userId) {
        return repository.findAllByUserId(userId);
    }

    @Transactional
    public void clear(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        repository.deleteByUserId(userId);
    }

    public @Nullable M findById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findByUserIdAndId(userId, id);
    }

    public @NotNull M findByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.findAllByUserId(userId).get(index);
    }

    @Transactional
    public void removeById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Transactional
    public void removeByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @Nullable String entityId = repository.findAllByUserId(userId).get(index).getId();
        if (entityId == null) throw new EmptyIdException();
        repository.deleteByUserIdAndId(userId, entityId);
    }

    public boolean existsById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) return false;
        return repository.findByUserIdAndId(userId, id) != null;
    }

    public boolean existsByIndex(@Nullable String userId, @NotNull Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return findByIndex(userId, index) != null;
    }

}
