package ru.tsc.kyurinova.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kyurinova.tm.api.service.dto.IDTOService;
import ru.tsc.kyurinova.tm.dto.model.AbstractEntityDTO;
import ru.tsc.kyurinova.tm.repository.dto.AbstractDTORepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDTOService<E extends AbstractEntityDTO> implements IDTOService<E> {

    @NotNull
    @Autowired
    private AbstractDTORepository<E> repository;

    @Transactional
    public void clear(){
        repository.deleteAll();
    }

    public List<E> findAll() {
        return repository.findAll();
    }

}

