package ru.tsc.kyurinova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.service.model.IService;
import ru.tsc.kyurinova.tm.dto.model.AbstractOwnerEntityDTO;

import java.sql.SQLException;
import java.util.List;

public interface IOwnerDTOService<E extends AbstractOwnerEntityDTO> extends IDTOService<E> {

    void remove(@Nullable String userId, @Nullable E entity) throws SQLException;

    @NotNull
    List<E> findAll(@Nullable String userId) throws SQLException;

    void clear(@Nullable String userId) throws SQLException;

    @Nullable
    E findById(@Nullable String userId, @Nullable String id) throws SQLException;

    @NotNull
    E findByIndex(@Nullable String userId, @Nullable Integer index) throws SQLException;

    void removeById(@Nullable String userId, @Nullable String id) throws SQLException;

    void removeByIndex(@Nullable String userId, @Nullable Integer index) throws SQLException;

    boolean existsById(@Nullable String userId, @Nullable String id) throws SQLException;

    boolean existsByIndex(@Nullable String userId, @NotNull Integer index) throws SQLException;

}
