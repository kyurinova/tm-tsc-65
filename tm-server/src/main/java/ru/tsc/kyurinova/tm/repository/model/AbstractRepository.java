package ru.tsc.kyurinova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.kyurinova.tm.model.AbstractModel;

import java.util.List;

@Repository
@Scope("prototype")
public interface AbstractRepository<E extends AbstractModel> extends JpaRepository<E, String> {

    void deleteAll();

    @NotNull
    List<E> findAll();

}

