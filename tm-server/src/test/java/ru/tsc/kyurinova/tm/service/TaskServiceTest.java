package ru.tsc.kyurinova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.kyurinova.tm.api.service.dto.IProjectDTOService;
import ru.tsc.kyurinova.tm.api.service.dto.IProjectTaskDTOService;
import ru.tsc.kyurinova.tm.api.service.dto.ITaskDTOService;
import ru.tsc.kyurinova.tm.api.service.dto.IUserDTOService;
import ru.tsc.kyurinova.tm.configuration.ServerConfiguration;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.dto.model.ProjectDTO;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;
import ru.tsc.kyurinova.tm.marker.UnitCategory;

import java.sql.SQLException;

public class TaskServiceTest {

    @NotNull
    private IUserDTOService userService;

    @NotNull
    private ITaskDTOService taskService;

    @NotNull
    private IProjectDTOService projectService;

    @NotNull
    private IProjectTaskDTOService projectTaskService;

    @NotNull
    private TaskDTO task;

    @NotNull
    private String taskId;

    @NotNull
    private final String taskName = "testTask";

    @NotNull
    private final String taskDescription = "testTaskDescription";

    @NotNull
    private ProjectDTO project;

    @NotNull
    private String projectId;

    @NotNull
    private String userId;

    public TaskServiceTest() throws SQLException {

    }

    @Before
    public void before() {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        projectService = context.getBean(IProjectDTOService.class);
        userService = context.getBean(IUserDTOService.class);
        taskService = context.getBean(ITaskDTOService.class);
        projectTaskService = context.getBean(IProjectTaskDTOService.class);
        userId = userService.create("test", "test").getId();
        project = projectService.create(userId, "project", "project");
        projectId = project.getId();
        task = taskService.create(userId, taskName, taskDescription);
        taskId = task.getId();
    }

    @Test
    @Category(UnitCategory.class)
    public void createByNameTest() throws SQLException {
        @NotNull final String newProjectName = "newTestProject";
        final String newTaskId = taskService.create(userId, newProjectName).getId();
        Assert.assertEquals(2, taskService.findAll().size());
        Assert.assertNotNull(taskService.findByName(userId, newProjectName));
        taskService.removeById(userId, newTaskId);
    }

    @Test
    @Category(UnitCategory.class)
    public void createTest() throws SQLException {
        @NotNull final String newProjectName = "newTestProject";
        @NotNull final String newProjectDescription = "newTestProjectDescription";
        final String newTaskId = taskService.create(userId, newProjectName, newProjectDescription).getId();
        Assert.assertEquals(2, taskService.findAll().size());
        Assert.assertNotNull(taskService.findByName(userId, newProjectName));
        Assert.assertEquals(newProjectDescription, taskService.findByName(userId, newProjectName).getDescription());
        taskService.removeById(userId, newTaskId);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByProjectTest() throws SQLException {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        Assert.assertNotNull(taskName);
        Assert.assertEquals(task.getId(), taskService.findById(userId, taskId).getId());
        Assert.assertEquals(task.getId(), taskService.findByIndex(userId, 0).getId());
        Assert.assertEquals(task.getId(), taskService.findByName(userId, taskName).getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void existsProjectTest() throws SQLException {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        Assert.assertTrue(taskService.existsById(userId, taskId));
        Assert.assertTrue(taskService.existsByIndex(userId, 0));
    }

    @Test
    @Category(UnitCategory.class)
    public void updateByIdTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        Assert.assertNotNull(taskName);
        Assert.assertNotNull(taskDescription);
        Assert.assertEquals(task.getId(), taskService.findByName(userId, taskName).getId());
        @NotNull final String newProjectName = "newTestProject";
        @NotNull final String newProjectDescription = "newTestProjectDescription";
        taskService.updateById(userId, taskId, newProjectName, newProjectDescription);
        Assert.assertEquals(task.getId(), taskService.findByName(userId, newProjectName).getId());
        @NotNull final TaskDTO newTask = taskService.findByName(userId, newProjectName);
        Assert.assertEquals(userId, newTask.getUserId());
        Assert.assertEquals(taskId, newTask.getId());
        Assert.assertEquals(newProjectName, newTask.getName());
        Assert.assertEquals(newProjectDescription, newTask.getDescription());
    }

    @Test
    @Category(UnitCategory.class)
    public void updateByIndexTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        Assert.assertNotNull(taskDescription);
        Assert.assertEquals(task.getId(), taskService.findById(userId, taskId).getId());
        @NotNull final String newProjectName = "newTestProject";
        @NotNull final String newProjectDescription = "newTestProjectDescription";
        taskService.updateByIndex(userId, 0, newProjectName, newProjectDescription);
        Assert.assertEquals(task.getId(), taskService.findByIndex(userId, 0).getId());
        @NotNull final TaskDTO newTask = taskService.findByIndex(userId, 0);
        Assert.assertEquals(userId, newTask.getUserId());
        Assert.assertEquals(taskId, newTask.getId());
        Assert.assertEquals(newProjectName, newTask.getName());
        Assert.assertEquals(newProjectDescription, newTask.getDescription());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTaskByIdTest() throws SQLException {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        taskService.removeById(userId, taskId);
        Assert.assertTrue(taskService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTaskByIndexTest() throws SQLException {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        taskService.removeByIndex(userId, 0);
        Assert.assertTrue(taskService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTaskByNameTest() throws SQLException {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        taskService.removeByName(userId, taskName);
        Assert.assertTrue(taskService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void startByIdTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        taskService.startById(userId, taskId);
        Assert.assertEquals(Status.IN_PROGRESS, taskService.findById(userId, taskId).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void startByIndexTest() throws SQLException {
        Assert.assertNotNull(userId);
        taskService.startByIndex(userId, 0);
        Assert.assertEquals(Status.IN_PROGRESS, taskService.findByIndex(userId, 0).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void startByNameTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        taskService.startByName(userId, taskName);
        Assert.assertEquals(Status.IN_PROGRESS, taskService.findByName(userId, taskName).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void finishByIdTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        taskService.finishById(userId, taskId);
        Assert.assertEquals(Status.COMPLETED, taskService.findById(userId, taskId).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void finishByIndexTest() throws SQLException {
        Assert.assertNotNull(userId);
        taskService.finishByIndex(userId, 0);
        Assert.assertEquals(Status.COMPLETED, taskService.findByIndex(userId, 0).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void finishByNameTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        taskService.finishByName(userId, taskName);
        Assert.assertEquals(Status.COMPLETED, taskService.findByName(userId, taskName).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void changeStatusByIdTest() throws SQLException {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        taskService.changeStatusById(userId, taskId, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, taskService.findById(userId, taskId).getStatus());
        taskService.changeStatusById(userId, taskId, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, taskService.findById(userId, taskId).getStatus());
        taskService.changeStatusById(userId, taskId, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, taskService.findById(userId, taskId).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void changeStatusByIndexTest() throws SQLException {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        taskService.changeStatusByIndex(userId, 0, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, taskService.findByIndex(userId, 0).getStatus());
        taskService.changeStatusByIndex(userId, 0, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, taskService.findByIndex(userId, 0).getStatus());
        taskService.changeStatusByIndex(userId, 0, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, taskService.findByIndex(userId, 0).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void changeStatusByNameTest() throws SQLException {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        taskService.changeStatusByName(userId, taskName, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, taskService.findByName(userId, taskName).getStatus());
        taskService.changeStatusByName(userId, taskName, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, taskService.findByName(userId, taskName).getStatus());
        taskService.changeStatusByName(userId, taskName, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, taskService.findByName(userId, taskName).getStatus());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void findByProjectAndTaskIdTest() throws SQLException {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        projectTaskService.bindTaskById(userId, projectId, taskId);
        Assert.assertEquals(task.getId(), taskService.findByProjectAndTaskId(userId, projectId, taskId).getId());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void bindTaskByIdTest() throws SQLException {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        task.setProjectId(projectId);
        projectTaskService.bindTaskById(userId, projectId, taskId);
        Assert.assertEquals(projectId, task.getProjectId());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void unbindTaskByIdTest() throws SQLException {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        projectTaskService.bindTaskById(userId, projectId, taskId);
        Assert.assertEquals(projectId, taskService.findById(userId, taskId).getProjectId());
        projectTaskService.unbindTaskById(userId, projectId, taskId);
        Assert.assertNull(taskService.findById(userId, taskId).getProjectId());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void removeProjectWithTasksByIdTest() throws SQLException {
        bindTaskByIdTest();
        Assert.assertEquals(1, projectTaskService.findAllTaskByProjectId(userId, projectId).size());
        projectTaskService.removeById(userId, projectId);
        Assert.assertEquals(0, projectTaskService.findAllTaskByProjectId(userId, projectId).size());
    }

    @SneakyThrows

    @Test
    @Category(UnitCategory.class)
    public void removeTasksFromProjectTest() throws SQLException {
        bindTaskByIdTest();
        Assert.assertEquals(1, projectTaskService.findAllTaskByProjectId(userId, projectId).size());
        projectTaskService.removeAllTaskByProjectId(userId, projectId);
        Assert.assertEquals(0, projectTaskService.findAllTaskByProjectId(userId, projectId).size());
    }

    @After
    public void after() throws SQLException {
        taskService.removeById(userId, taskId);
        projectService.removeById(userId, projectId);
        userService.removeById(userId);
    }

}
