package ru.tsc.kyurinova.tm.repository.DTO;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;

import java.util.List;

public interface TaskDTORepository extends JpaRepository<TaskDTO, String> {

    @NotNull
    List<TaskDTO> findByProjectId(@NotNull final String projectId);

}
