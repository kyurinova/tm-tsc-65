package ru.tsc.kyurinova.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.tsc.kyurinova.tm")
public class ApplicationConfiguration {
}
