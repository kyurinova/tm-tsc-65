package ru.tsc.kyurinova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectRestEndpoint {

    @Nullable
    List<ProjectDTO> findAll() throws Exception;

    void save(@NotNull ProjectDTO project) throws Exception;

    @Nullable
    ProjectDTO findById(@NotNull String id) throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    long count() throws Exception;

    void deleteById(@NotNull String id) throws Exception;

    void delete(@NotNull ProjectDTO project) throws Exception;

    void clear() throws Exception;

}
