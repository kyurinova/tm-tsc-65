package ru.tsc.kyurinova.tm.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.kyurinova.tm.model.Project;

public interface ProjectRepository extends JpaRepository<Project, String> {

}
