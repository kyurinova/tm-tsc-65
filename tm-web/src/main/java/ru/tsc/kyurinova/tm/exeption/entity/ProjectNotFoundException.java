package ru.tsc.kyurinova.tm.exeption.entity;

import ru.tsc.kyurinova.tm.exeption.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error. Project not found.");
    }

}
