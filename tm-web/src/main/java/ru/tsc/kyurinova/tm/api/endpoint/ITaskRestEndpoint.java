package ru.tsc.kyurinova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRestEndpoint {

    @Nullable
    List<TaskDTO> findAll() throws Exception;

    void save(@NotNull TaskDTO project) throws Exception;

    @Nullable
    TaskDTO findById(@NotNull String id) throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    long count() throws Exception;

    void deleteById(@NotNull String id) throws Exception;

    void delete(@NotNull TaskDTO project) throws Exception;

    void clear() throws Exception;

}
