package ru.tsc.kyurinova.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.kyurinova.tm.api.service.model.IProjectService;

@Controller
public class ProjectsController {

    @Autowired
    private IProjectService projectService;

    @GetMapping("/projects")
    public ModelAndView index() throws Exception {
        return new ModelAndView("project-list", "projects", projectService.findAll());
    }

}
