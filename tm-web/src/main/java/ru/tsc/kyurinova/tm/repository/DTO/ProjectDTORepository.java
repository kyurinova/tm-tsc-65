package ru.tsc.kyurinova.tm.repository.DTO;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.kyurinova.tm.dto.model.ProjectDTO;

public interface ProjectDTORepository extends JpaRepository<ProjectDTO, String> {

}
